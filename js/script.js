class Visit {
    constructor(doctor, visitDate, name, purpose) {
        this.doctor = doctor;
        this.visitDate = visitDate;
        this.name = name;
        this.purpose = purpose;
    }
}

class Cardiologist extends Visit {
    constructor(doctor, visitDate, name, purpose, age, pressure, massIndex, heartIssues) {
        super(doctor, visitDate, name, purpose);
        this.age = age;
        this.pressure = pressure;
        this.massIndex = massIndex;
        this.heartIssues = heartIssues;
    }
}

class Dentist extends Visit {
    constructor(doctor, visitDate, name, purpose, lastVisitDate) {
        super(doctor, visitDate, name, purpose);
        this.lastVisitDate = lastVisitDate;
    }
}

class Therapist extends Visit {
    constructor(doctor, visitDate, name, purpose, age) {
        super(doctor, visitDate, name, purpose);
        this.age = age;
    }
}

let createButton = document.querySelector('.create-button');
let bodyWrapper = document.querySelector('.wrapper');
let registerDesk = document.querySelector('.register-desk');

let dataArray = [];
let selectedDoctor;

/*-----------------------------LOCAL STORAGE--------------------------------------------*/
if (localStorage.getItem('registerData')) {
    dataArray = JSON.parse(localStorage.getItem("registerData"));
    dataArray.forEach(function (object) {
        showCard (object)
    })
} else {
    dataArray.forEach(function (item) {
        item.remove()
    });
}
/*-----------------------------CREATE MODAL WINDOW--------------------------------------------*/

createButton.addEventListener('click', function () {
    createModalWindow();
});

/*-----------------------------SUPPLEMENT FUNCTIONS--------------------------------------------*/

function chooseCardiologist() {
    let cardiologistForm = document.querySelector('.user-form');

    cardiologistForm.innerHTML = `
            <input id = "visitDate" type="date" data-date-format="DD-MM-YYYY" required placeholder="Visit date">
            <input id = "purpose" type="text" required placeholder="Visit purpose">
            <input id = "name" type="text" required placeholder="Name">
            <input id = "age" type="number" required placeholder="Age">
            <input id = "pressure" type="number" required placeholder="Usual pressure">
            <input id = "massIndex" type="number" required placeholder="Mass index">
            <input id = "heartIssues" type="text" required placeholder="Heart issues">`;
}

function chooseDentist() {
    let dentistForm = document.querySelector('.user-form');

    dentistForm.innerHTML = `
            <input id = "visitDate" type="date" data-date-format="DD-MM-YYYY" required placeholder="Visit date">
            <input id = "purpose" type="text" required placeholder="Visit purpose">
            <input id = "name" type="text" required placeholder="Name">
            <input id = "lastVisitDate" type="date" data-date-format="DD-MM-YYYY" required placeholder="Date of last visit">`;
}

function chooseTherapist() {
    let therapistForm = document.querySelector('.user-form');

    therapistForm.innerHTML = `
            <input id = "visitDate" type="date" data-date-format="DD-MM-YYYY" required placeholder="Visit date">
            <input id = "purpose" type="text" required placeholder="Visit purpose">
            <input id = "name" type="text" required placeholder="Name">
            <input id = "age" type="number" required placeholder="Age">`;
}

function createModalWindow() {
    let modalWindow = document.createElement('div');
    let modalWindowCloseButton = document.createElement('button');
    let registerForm = document.createElement('form');
    let registerButton = document.createElement('input');

    registerButton.setAttribute("type", "submit");
    registerButton.setAttribute("value", "Submit");
    modalWindow.className = 'modal-window';
    modalWindowCloseButton.className = 'close-button';
    modalWindowCloseButton.innerText = 'x';
    registerForm.className = 'register-form';
    registerButton.className = 'register-button';
    registerForm.innerHTML = `
            <select class="doctor-options">
                <option value="choose a doctor">Choose a doctor</option>
                <option value="cardiologist">Cardiologist</option>
                <option value="dentist">Dentist</option>
                <option value="therapist">Therapist</option>
            </select>
            <div class="user-form"></div>
            <textarea maxlength="400" placeholder = "Comments" class="comments-input"></textarea>`;

    modalWindow.appendChild(modalWindowCloseButton);
    modalWindow.appendChild(registerForm);
    registerForm.appendChild(registerButton);
    bodyWrapper.appendChild(modalWindow);

    let doctorOptions = document.querySelector('.doctor-options');

    doctorOptions.addEventListener('change', selectDoctor);

    modalWindowCloseButton.addEventListener('click', function () {
        bodyWrapper.removeChild(modalWindow)
    });

    registerForm.addEventListener('submit', function (event) {
        event.preventDefault();

        let newCard;

        if (selectedDoctor === 'Cardiologist'){
            newCard = new Cardiologist(selectedDoctor, document.getElementById('visitDate').value, document.getElementById('name').value, document.getElementById('purpose').value, document.getElementById('age').value, document.getElementById('pressure').value, document.getElementById('massIndex').value, document.getElementById('heartIssues').value);

            dataArray.push(newCard);

        } else if (selectedDoctor === 'Dentist'){
            newCard = new Dentist (selectedDoctor, document.getElementById('visitDate').value, document.getElementById('name').value, document.getElementById('purpose').value, document.getElementById('lastVisitDate').value);

            dataArray.push(newCard);

        } else if (selectedDoctor === 'Therapist') {
            newCard = new Therapist (selectedDoctor, document.getElementById('visitDate').value, document.getElementById('name').value, document.getElementById('purpose').value, document.getElementById('age').value);

            dataArray.push(newCard);

        } else {

            return console.log('please choose a doctor')
        }

        showCard (newCard);
        bodyWrapper.removeChild(modalWindow);

        localStorage.setItem("registerData", JSON.stringify(dataArray));
    });
}

function selectDoctor() {
    let userForm = document.querySelector('.user-form');
    let doctorOptions = document.querySelector('.doctor-options');
    selectedDoctor = doctorOptions[doctorOptions.selectedIndex].text;

    if (selectedDoctor === 'Cardiologist'){
        chooseCardiologist()
    } else if (selectedDoctor === 'Dentist'){
        chooseDentist()
    } else if (selectedDoctor === 'Therapist'){
        chooseTherapist()
    } else {
        userForm.innerHTML = ''
    }
};

function checkDeskItems() {
    let noItemsMessage = document.querySelector('.register-text');

    if (registerDesk.childElementCount !== 0) {
        noItemsMessage.hidden = true;
    } else {
        noItemsMessage.hidden = false;
    }
}

function findCardIndex(event) {
    let cardsArray = Array.from(registerDesk.children);
    // console.log(cardsArray);

    let foundCardItem = cardsArray.find(function(item){
        return item === event.target.parentNode
    });
    // console.log(foundCardItem);

    return cardsArray.indexOf(foundCardItem);
};

function showCard (object) {
    let registerNote = document.createElement('div');
    let registerNoteCloseButton = document.createElement('button');

    registerNote.className = 'register-note';
    registerNoteCloseButton.className = 'close-button';
    registerNoteCloseButton.innerText = 'x';

    let titleContainer = document.createElement('div');
    titleContainer.className = 'title-container';
    let nameTitle = document.createElement('p');
    let doctorTitle = document.createElement('p');
    let showMoreButton = document.createElement('a');
    nameTitle.innerText = object.name;
    nameTitle.className = 'name-title';
    doctorTitle.innerText = object.doctor;
    doctorTitle.className = 'doctor-title';
    showMoreButton.innerText = 'Show more';
    showMoreButton.className = 'show-more-button';

    registerNote.appendChild(registerNoteCloseButton);
    titleContainer.appendChild(nameTitle);
    titleContainer.appendChild(doctorTitle);
    registerNote.appendChild(titleContainer);
    registerNote.appendChild(showMoreButton);
    registerDesk.appendChild(registerNote);

    checkDeskItems();

    registerNoteCloseButton.addEventListener('click', function (event) {
        let foundIndex = findCardIndex(event);
        dataArray.splice(foundIndex, 1);
        // console.log(dataArray);
        registerDesk.removeChild(registerNote);
        checkDeskItems();

        localStorage.setItem("registerData", JSON.stringify(dataArray));
    });

    showMoreButton.addEventListener('click', function (event) {
        let foundCardIndex = findCardIndex(event);
        let foundElement = dataArray[foundCardIndex];

        for(let key in foundElement) {
            if (key !== 'doctor' && key !== 'name') {

                let newElement = document.createElement('p');
                newElement.className = 'extra-info-text';
                newElement.innerHTML = `${key}: ${foundElement[key]}`;

                registerNote.insertBefore(newElement, showMoreButton);
            }
        };
        showMoreButton.style.display = 'none';

        let hideInfoButton = document.createElement('a');
        hideInfoButton.className = 'hide-info-button';
        hideInfoButton.innerText = 'Hide Information';

        registerNote.appendChild(hideInfoButton);

        hideInfoButton.addEventListener('click', function () {
            let extraInfo = registerNote.querySelectorAll('.extra-info-text');
            extraInfo.forEach(function (element) {
                element.remove()
            });

            hideInfoButton.remove();
            showMoreButton.style.display = 'block';
        })
    });

    /*-------------------------------------DRAG'n'DROP-------------------------------------*/

        let shiftX = 0;
        let shiftY = 0;
        let currentCursorPositionX;
        let currentCursorPositionY;
        let elementPositionX = registerNote.offsetLeft;
        let elementPositionY = registerNote.offsetTop;

    registerNote.addEventListener('mousedown', function (e) {
            if (e.target !== registerNoteCloseButton && e.target !== showMoreButton) {
                let coordinateX = e.pageX;
                let coordinateY = e.pageY;

                registerNote.style.zIndex = '10';

                document.onmousemove = function (e) {
                    currentCursorPositionX = e.pageX - coordinateX + shiftX;
                    currentCursorPositionY = e.pageY - coordinateY + shiftY;

                    let elementOffsetPositionX = currentCursorPositionX + elementPositionX;
                    let elementOffsetPositionY = currentCursorPositionY + elementPositionY;

                    if (elementOffsetPositionX > 0 && elementOffsetPositionX < 900 && elementOffsetPositionY > 0 && elementOffsetPositionY < 610) {
                        registerNote.style.left = px(currentCursorPositionX);
                        registerNote.style.top = px(currentCursorPositionY);
                    }
                };

                document.onmouseup = function () {
                    shiftX = parseInt(registerNote.style.left);
                    shiftY = parseInt(registerNote.style.top);

                    document.onmousemove = null;
                    registerNote.style.zIndex = '1';
                }
            }
        });
        function px(c) {
            return c + 'px'
        }
};










